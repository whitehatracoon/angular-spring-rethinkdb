import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Task} from '../../model/task';
import {BaseService} from '../base.service';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class TaskService extends BaseService {

  constructor(http: Http) {
    super(http);
  }

  createTasks(userId: String, title: String, text: String) {
    const task = new Task(title, text);
    this.http.post('http://localhost:8080/users/' + userId + '/tasks', task, {headers: this.headers}).subscribe(
      data => {},
      error => console.error(error)
    );
  }

  getTasks(userId: string): Observable<any> {
    return this.http.get('http://localhost:8080/users/' + userId + '/tasks');
  }

  updateTaskState(taskKey: String, userId: string, state: boolean): Observable<any> {
    return this.http.patch('http://localhost:8080/users/' + userId + '/tasks/' + taskKey, {value: state}, {headers: this.headers});
  }

  deleteTask(taskKey: string, userId: string): Observable<any> {
    return this.http.delete('http://localhost:8080/users/' + userId + '/tasks/' + taskKey);
  }
}
