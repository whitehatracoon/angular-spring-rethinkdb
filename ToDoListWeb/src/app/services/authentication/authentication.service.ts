import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import {BaseService} from '../base.service';
import {StorageModule} from '../../moduls/storage/storage.module';

@Injectable()
export class AuthenticationService extends BaseService {

  constructor(http: Http, private storage: StorageModule) {
    super(http);
  }

  public login(userName: string, password: string) {
    return this.http.post('http://localhost:8080/users/login', {userName: userName, password: password}, {headers: this.headers});
  }

  public logout() {
    this.storage.removeValue(this.userIdKey);
  }

  public loggedIn(): boolean {
    if ( this.storage.getValue(this.userIdKey) ) return true;
    return false;
  }

}
