import { Injectable } from '@angular/core';
import {StompService} from '@stomp/ng2-stompjs';
import {Message} from '@stomp/stompjs';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class WebSocketService {

  constructor(private service: StompService) {

  }

  subscribeTo(destination: string): Observable<any> {
    return this.service.subscribe(destination)
      .map((message: Message) => message.body);
  }
}
