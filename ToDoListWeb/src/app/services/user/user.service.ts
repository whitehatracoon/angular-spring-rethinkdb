import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {User} from '../../model/user';
import {BaseService} from '../base.service';

@Injectable()
export class UserService extends BaseService {

  constructor(http: Http) {
    super(http);
  }

  create(userName: string, email: string, password: string) {
    const self = this;
    const user = new User(userName, email, password);
    return this.http.post('http://localhost:8080/users/register', JSON.stringify(user), {headers: self.headers});
  }

}
