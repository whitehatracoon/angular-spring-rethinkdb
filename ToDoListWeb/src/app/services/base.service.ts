import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';

@Injectable()
export abstract class BaseService {
  protected headers: Headers = new Headers({'Content-Type': 'application/json'});
  protected userIdKey = 'userId';

  constructor(protected http: Http) { }

}
