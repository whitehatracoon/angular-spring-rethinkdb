import { Component } from '@angular/core';

import { Task } from './model/task';
import {AuthenticationService} from './services/authentication/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'To Do List';

  constructor(public authService: AuthenticationService) {}
}
