import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthenticationService} from '../../services/authentication/authentication.service';
import {StorageModule} from '../../moduls/storage/storage.module';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {
  loginForm: FormGroup;

  constructor(private formBuilder: FormBuilder, public authService: AuthenticationService,
              private storage: StorageModule, private router: Router) {
    this.createForm();
  }

  ngOnInit() {
  }

  onSubmit() {
    if (this.loginForm.valid) {
      const userName = this.loginForm.value.userName;
      const password = this.loginForm.value.password;
      const self = this;
      this.authService.login(userName, password).subscribe(
          data => {
            self.storage.storeValue('userId', data.text());
            self.router.navigate(['/tasks']);
          },
          error => console.error(error)
        );
    }
  }

  logout() {
    this.authService.logout();
  }

  createForm() {
    this.loginForm = this.formBuilder.group({
      userName: ['', [Validators.required, Validators.minLength(3)] ],
      password: ['', [Validators.required, Validators.minLength(3)]],
    });
  }

}
