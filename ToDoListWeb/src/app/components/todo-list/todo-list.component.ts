/**
 * Created by venance on 11.06.17.
 */
import {Component, Input, OnDestroy, OnInit} from '@angular/core';

import { Task } from '../../model/task';
import {TaskService} from '../../services/task/task.service';
import {AuthenticationService} from '../../services/authentication/authentication.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {StorageModule} from '../../moduls/storage/storage.module';
import {WebSocketService} from '../../services/web-socket/web-socket.service';
import {Subscription} from 'rxjs/Subscription';
import {forEach} from '@angular/router/src/utils/collection';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit, OnDestroy {
  tasks: Task[];
  taskForm: FormGroup;
  private userId: string;
  private subscription: Subscription;

  constructor(private taskService: TaskService,  public authService: AuthenticationService,
              private storage: StorageModule, private formBuilder: FormBuilder,
              private webSocketService: WebSocketService) {
    if ( authService.loggedIn() ) {
      const self = this;
      self.createForm();
      self.userId = storage.getValue('userId');
      taskService.getTasks(self.userId).subscribe(
        data => {
          if ( data.ok ) self.tasks = data.json();
        },
        error => console.error(error)
      );
    }
  }

  ngOnInit() {
    if (!this.subscription) {
      const self = this;
      this.subscription = this.webSocketService.subscribeTo('/tasks/' + this.userId)
        .subscribe((taskJson: string) => {
          const task: Task = JSON.parse(taskJson);
          self.addToTasks(task);
        });
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.subscription = null;
  }

  private addToTasks(task: Task) {
    if (!this.inTasks(task)) this.tasks.push(task);
  }

  private inTasks(task: Task): boolean {
    for (const t of this.tasks) if ( t.id === task.id ) return true;
    return false;
  }

  toggleState(task: Task) {
    const state = !task.done;
    this.taskService.updateTaskState(task.id, this.userId, state).subscribe(
      data =>  {
        if (data.json() === true && data.ok) task.done = state;
      },
      error => console.error(error)
    );
  }

  removeTask(taskKey: string) {
    const self = this;
    this.taskService.deleteTask(taskKey, this.userId).subscribe(
      data => {
        if (data.json()) self.deleteTask(taskKey);
      },
      error => console.error(error.body)
    );
  }

  deleteTask(taskKey: string) {
    const self = this;
    self.tasks.forEach(function (element, index) {
      if (taskKey === element.id) {
        self.tasks.splice(index, 1);
      }
    });
  }

  onSubmit() {
    if (this.taskForm.valid) {
      const title = this.taskForm.value.taskTitle;
      const text = this.taskForm.value.taskBody;
      this.taskService.createTasks(this.userId, title, text);
    }
  }

  private createForm() {
    this.taskForm = this.formBuilder.group({
      taskTitle: ['', Validators.required ],
      taskBody: ['', Validators.required ],
    });
  }
}
