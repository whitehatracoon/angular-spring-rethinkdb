import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {UserService} from '../../services/user/user.service';
import {StorageModule} from '../../moduls/storage/storage.module';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent implements OnInit {
  registerForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private userService: UserService,
              private storage: StorageModule, private router: Router) {
    this.createForm();
  }

  ngOnInit() {
  }

  onSubmit() {
    const username = this.registerForm.value.userName;
    const email = this.registerForm.value.email;
    const password = this.registerForm.value.password;
    const self = this;
    this.userService.create(username, email, password).subscribe(
        data => {
          self.storage.storeValue('userId', data.text());
          self.router.navigate(['/tasks']);
        },
        error => console.error(error)
      );
  }
  createForm() {
    this.registerForm = this.formBuilder.group({
      userName: ['', [Validators.required, Validators.minLength(3)] ],
      email: ['', [Validators.email, Validators.minLength(6)] ],
      password: ['', [Validators.required, Validators.minLength(3)]],
    });
  }

}
