import {Task} from './task';
/**
 * Created by venance on 03.07.17.
 */

export class User {
  userId: string;
  userName: string;
  email: string;
  password: string;
  tasks: any;

  constructor(userName: string, email: string, password: string) {
    this.userId = null;
    this.userName = userName;
    this.email = email;
    this.password = password;
    this.tasks = {};
  }
}
