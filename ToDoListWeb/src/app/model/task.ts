/**
 * Created by venance on 11.06.17.
 */
export class Task {
  id: String;
  title: String;
  text: String;
  done: boolean;

  constructor(title: String, text: String) {
    this.title = title;
    this.text = text;
    this.done = false;
  }
}
