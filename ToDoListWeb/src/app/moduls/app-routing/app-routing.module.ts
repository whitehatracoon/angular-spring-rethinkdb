import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {PageNotFoundComponent} from '../../components/page-not-found/page-not-found.component';
import {HomeComponent} from '../../components/home/home.component';
import {TodoListComponent} from '../../components/todo-list/todo-list.component';
import {RegisterFormComponent} from '../../components/register-form/register-form.component';
import {LoginFormComponent} from '../../components/login-form/login-form.component';

const appRoutes: Routes = [
  {path: 'register', component: RegisterFormComponent },
  {path: 'login', component: LoginFormComponent },
  {path: 'tasks', component: TodoListComponent},
  {path: '', pathMatch: 'full', component: HomeComponent },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
