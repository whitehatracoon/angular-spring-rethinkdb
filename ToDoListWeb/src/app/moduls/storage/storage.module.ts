import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: []
})
export class StorageModule {

  public storeValue(key: string, value: any) {
    if ( typeof value !== 'string' ) value = JSON.stringify(value);
    localStorage.setItem(key, value);
  }

  public getValue(key: string): any {
    return localStorage.getItem(key);
  }

  public removeValue(key: string) {
    localStorage.removeItem(key);
  }
}
