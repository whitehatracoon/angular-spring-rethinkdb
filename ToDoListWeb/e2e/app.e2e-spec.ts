import { ToDoListWebPage } from './app.po';

describe('to-do-list-web App', () => {
  let page: ToDoListWebPage;

  beforeEach(() => {
    page = new ToDoListWebPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
