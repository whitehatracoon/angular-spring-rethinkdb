package de.whitehatracoons.todoList.model.user;

import de.whitehatracoons.todoList.model.task.Task;
import de.whitehatracoons.todoList.util.TaskKeyGenerator;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by venance on 24.06.17.
 */
public class User {
    private String userId;
    private String userName;
    private String email;
    private String password;
    private Map<String, Task> tasks;

    public User() {
    }

    public User(String userName, String email, String password) {
        this.userName = userName;
        this.email = email;
        this.password = password;
        this.tasks = new HashMap<>();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Map<String, Task> getTasks() {
        return tasks;
    }

    public void setTasks(Map<String, Task> tasks) {
        this.tasks = tasks;
    }

    public String addTask(Task task) {
        TaskKeyGenerator keyGenerator = new TaskKeyGenerator();
        String taskKey = keyGenerator.generateTaskKey();
        task.setId(taskKey);
        this.tasks.put(task.getId(), task);
        return task.getId();
    }

    public void removeTask(String taskKey) {
        this.tasks.remove(taskKey);
    }
}
