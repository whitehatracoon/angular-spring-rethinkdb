package de.whitehatracoons.todoList.model.task;

/**
 * Created by venance on 01.06.17.
 */
public class Task {

    private String id;

    private String title;

    private String text;

    private boolean isDone;

    public Task() {
    }

    public Task(String title, String text) {
        this.title = title;
        this.text = text;
        this.isDone = false;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public String getText() {
        return this.text;
    }

    public boolean getDone() {
        return isDone;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setDone(boolean done) {
        this.isDone = done;
    }
}
