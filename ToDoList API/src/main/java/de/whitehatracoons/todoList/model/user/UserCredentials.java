package de.whitehatracoons.todoList.model.user;

/**
 * Created by venance on 05.07.17.
 */
public class UserCredentials {
    private String userName;
    private String password;

    public UserCredentials() {} // for json mapping

    public UserCredentials(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

}
