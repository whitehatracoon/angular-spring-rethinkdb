package de.whitehatracoons.todoList.model.state;

/**
 * Created by venance on 06.07.17.
 */
public class State {
    private boolean value;

    public boolean getValue() {
        return value;
    }

    public void setValue(boolean value) {
        this.value = value;
    }
}
