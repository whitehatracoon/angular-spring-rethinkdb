package de.whitehatracoons.todoList.util.validators;

import de.whitehatracoons.todoList.model.task.Task;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by venance on 30.06.17.
 */
public class TaskValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return Task.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "title", "title.empty");
        ValidationUtils.rejectIfEmpty(errors, "text", "text.empty");
        Task task = (Task) target;
        if ( task.getId() != null ) errors.rejectValue("id", "id must be null");
        if ( task.getDone() ) errors.rejectValue("done", "done must be false");
    }
}
