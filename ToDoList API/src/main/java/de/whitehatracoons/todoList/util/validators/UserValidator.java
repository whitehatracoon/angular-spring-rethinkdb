package de.whitehatracoons.todoList.util.validators;

import de.whitehatracoons.todoList.model.user.User;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by venance on 30.06.17.
 */
public class UserValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return User.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "userName", "userName.empty");
        ValidationUtils.rejectIfEmpty(errors, "email", "email.empty");
        ValidationUtils.rejectIfEmpty(errors, "password", "password.empty");
        User user = (User) target;
        if (user.getUserId() != null) errors.rejectValue("userId","user id must be empty");
        if (!user.getTasks().isEmpty()) errors.rejectValue("tasks","tasks must be empty");
        if (user.getUserName().length() < 3) errors.rejectValue("userName","user name too short");
        if (user.getPassword().length() < 3) errors.rejectValue("password","password too short");
        if (!user.getEmail().matches("\\w+@\\w+.de")) errors.rejectValue("email","email wrong format");
    }
}
