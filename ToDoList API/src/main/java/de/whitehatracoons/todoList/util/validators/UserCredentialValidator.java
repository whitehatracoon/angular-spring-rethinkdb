package de.whitehatracoons.todoList.util.validators;

import de.whitehatracoons.todoList.model.user.UserCredentials;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by venance on 06.07.17.
 */
public class UserCredentialValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return UserCredentials.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "userName", "userName.empty");
        ValidationUtils.rejectIfEmpty(errors, "password", "password.empty");
    }
}
