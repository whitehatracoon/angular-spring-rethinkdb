package de.whitehatracoons.todoList.util;

import java.util.Random;

/**
 * Created by venance on 24.06.17.
 */
public class TaskKeyGenerator {
    private static final char[] symbols;

    static {
        StringBuilder tmp = new StringBuilder();
        for (char ch = '0'; ch <= '9'; ch++) {
            tmp.append(ch);
        }
        for (char ch = 'a'; ch <= 'z'; ch++) {
            tmp.append(ch);
        }
        symbols = tmp.toString().toCharArray();
    }

    private final Random random;
    private final char[] buf;

    public TaskKeyGenerator() {
        random  = new Random();
        buf = new char[10];
    }

    public String generateTaskKey() {
        for (int i = 0; i < buf.length; i++) {
            buf[i] = symbols[random.nextInt(symbols.length)];
        }
        return new String(buf);
    }
}
