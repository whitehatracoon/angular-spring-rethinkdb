package de.whitehatracoons.todoList.util;

import de.whitehatracoons.todoList.entity.user.UserEntity;
import de.whitehatracoons.todoList.model.user.User;
import org.dozer.DozerBeanMapper;
import org.dozer.loader.api.BeanMappingBuilder;
import org.springframework.stereotype.Component;

/**
 * Created by venance on 01.06.17.
 */
@Component
public class Mapper extends DozerBeanMapper {

    private Mapper() {
        super();
        this.addMapping(beanMappingBuilder());
    }

    private BeanMappingBuilder beanMappingBuilder() {
        return new BeanMappingBuilder() {
            protected void configure() {
                mapping(UserEntity.class, User.class);
            }
        };
    }
}
