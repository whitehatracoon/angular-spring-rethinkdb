package de.whitehatracoons.todoList.controller.task;

import de.whitehatracoons.todoList.exception.task.TaskStateNotUpdatedException;
import de.whitehatracoons.todoList.exception.validation.ValidationException;
import de.whitehatracoons.todoList.model.state.State;
import de.whitehatracoons.todoList.model.task.Task;
import de.whitehatracoons.todoList.service.user.UserService;
import de.whitehatracoons.todoList.util.validators.TaskValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by venance on 10.07.17.
 */
@RestController
@RequestMapping("/users")
public class TaskController {

    private final UserService userService;

    @Autowired
    public TaskController(UserService userService) {
        this.userService = userService;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping(path = "/{userId}/tasks")
    @ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody String addTask(@PathVariable String userId, @RequestBody Task task, BindingResult result) {
        TaskValidator validator = new TaskValidator();
        validator.validate(task, result);
        if (result.hasErrors()) throw new ValidationException(result.toString());
        return userService.addTaskToUser(userId, task);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path = "/{userId}/tasks")
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody
    List<Task> getTasks(@PathVariable String userId) {
        return userService.getAllTasksForUser(userId);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PatchMapping(path = "/{userId}/tasks/{taskKey}")
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody Boolean updateTaskState(@PathVariable String userId, @PathVariable String taskKey, @RequestBody State state) throws TaskStateNotUpdatedException{
        return userService.updateUserTaskState(userId, taskKey, state.getValue());
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping(path = "/{userId}/tasks/{taskKey}")
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody Boolean updateTaskState(@PathVariable String userId, @PathVariable String taskKey) {
        return userService.deleteUserTask(userId, taskKey);
    }
}
