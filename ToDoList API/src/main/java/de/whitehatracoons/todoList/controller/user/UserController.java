package de.whitehatracoons.todoList.controller.user;

import de.whitehatracoons.todoList.exception.user.FailedLoginException;
import de.whitehatracoons.todoList.exception.validation.ValidationException;
import de.whitehatracoons.todoList.exception.user.UserNotFoundException;
import de.whitehatracoons.todoList.listeners.TaskChangesListener;
import de.whitehatracoons.todoList.model.user.User;
import de.whitehatracoons.todoList.model.user.UserCredentials;
import de.whitehatracoons.todoList.service.user.UserService;
import de.whitehatracoons.todoList.util.validators.UserCredentialValidator;
import de.whitehatracoons.todoList.util.validators.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

/**
 * Created by venance on 25.06.17.
 */
@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;
    private final TaskChangesListener taskChangesListener;

    @Autowired
    public UserController(UserService userService, TaskChangesListener taskChangesListener) {
        this.userService = userService;
        this.taskChangesListener = taskChangesListener;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping(path = "/register")
    @ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody String registerUser(@RequestBody User user, BindingResult result) {
        UserValidator validator = new UserValidator();
        validator.validate(user, result);
        if (result.hasErrors()) throw new ValidationException(result.toString());
        return userService.storeUser(user);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping(path = "/login")
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody String login(@RequestBody UserCredentials userCredentials, BindingResult result) throws UserNotFoundException {
        UserCredentialValidator validator = new UserCredentialValidator();
        validator.validate(userCredentials, result);
        if (result.hasErrors()) throw new ValidationException(result.toString());
        String userId = userService.handleLogin(userCredentials);
        if ( userId == null ) throw new FailedLoginException("wrong user password");
        this.taskChangesListener.pushChangesToWebSocket(userId);
        return userId;
    }

    @GetMapping(path = "/{userId}")
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody User getUser(@PathVariable String userId) {
        return userService.findUserById(userId);
    }

    @DeleteMapping(path = "/{userId}")
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody Boolean unregisterUser(@PathVariable String userId) {
        return userService.deleteUser(userId);
    }
}
