package de.whitehatracoons.todoList.exception.task;

/**
 * Created by venance on 28.06.17.
 */
public class TaskNotAddedException extends RuntimeException {
    public TaskNotAddedException(String message) {
        super(message);
    }
}
