package de.whitehatracoons.todoList.exception.task;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by venance on 24.06.17.
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class TaskStateNotUpdatedException extends RuntimeException {
    public TaskStateNotUpdatedException(String message) {
        super(message);
    }
}
