package de.whitehatracoons.todoList.exception.user;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by venance on 06.07.17.
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class FailedLoginException extends RuntimeException {
    public FailedLoginException(String message) {
        super(message);
    }
}
