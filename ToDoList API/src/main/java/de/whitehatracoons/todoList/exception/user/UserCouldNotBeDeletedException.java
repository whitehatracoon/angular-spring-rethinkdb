package de.whitehatracoons.todoList.exception.user;

/**
 * Created by venance on 24.06.17.
 */
public class UserCouldNotBeDeletedException extends RuntimeException {
    public UserCouldNotBeDeletedException() {
        super();
    }
}
