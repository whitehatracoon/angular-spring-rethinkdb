package de.whitehatracoons.todoList.service.user;

import de.whitehatracoons.todoList.database.DatabaseConnector;
import de.whitehatracoons.todoList.entity.user.UserEntity;
import de.whitehatracoons.todoList.exception.task.TaskNotFoundException;
import de.whitehatracoons.todoList.exception.task.TaskStateNotUpdatedException;
import de.whitehatracoons.todoList.exception.user.UserCouldNotBeDeletedException;
import de.whitehatracoons.todoList.exception.user.UserNotFoundException;
import de.whitehatracoons.todoList.model.task.Task;
import de.whitehatracoons.todoList.model.user.User;
import de.whitehatracoons.todoList.model.user.UserCredentials;
import de.whitehatracoons.todoList.util.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by venance on 24.06.17.
 */
@Service
@Scope("singleton")
public class UserServiceImpl implements UserService {

    private Mapper mapper;
    private DatabaseConnector databaseConnector;

    @Autowired
    private UserServiceImpl(DatabaseConnector databaseConnector, Mapper mapper) {
        this.databaseConnector = databaseConnector;
        this.mapper = mapper;
    }

    @Override
    public List<User> findAll() {
        List<UserEntity> userEntities = databaseConnector.findAllUsers();
        List<User> users = new ArrayList<>();
        for (UserEntity entity : userEntities) {
            users.add(mapper.map(entity, User.class));
        }
        return users;
    }

    @Override
    public User findUserById(String userId) {
        return mapper.map(databaseConnector.findUserById(userId), User.class);
    }

    @Override
    public String storeUser(User user) {
        UserEntity userEntity = mapper.map(user, UserEntity.class);
        return databaseConnector.storeUser(userEntity);
    }

    @Override
    public String addTaskToUser(String userId, Task task) {
        return databaseConnector.addTaskToUser(userId, task);
    }

    @Override
    public List<Task> getAllTasksForUser(String userId) {
        List<Task> tasks = databaseConnector.findAllTasksForUser(userId);
        return tasks;
    }

    @Override
    public boolean updateUserTaskState(String userId, String taskKey, boolean state) throws TaskStateNotUpdatedException {
        return databaseConnector.updateUserTaskState(userId, taskKey, state);
    }

    @Override
    public boolean deleteUserTask(String userId, String taskKey) throws TaskNotFoundException {
        return databaseConnector.deleteUserTask(userId, taskKey);
    }

    @Override
    public boolean deleteUser(String userId) {
        try {
            return databaseConnector.deleteUser(userId);
        } catch (UserCouldNotBeDeletedException ex) {
            return false;
        }
    }

    @Override
    public String handleLogin(UserCredentials userCredentials) throws UserNotFoundException {
        Map<String, String> userToVerify = databaseConnector.returnUserToVerify(userCredentials);
        if ( userCredentials.getUserName().equals(userToVerify.get("userName")) && userCredentials.getPassword().equals(userToVerify.get("password")) ) return userToVerify.get("id");
        return null;
    }
}
