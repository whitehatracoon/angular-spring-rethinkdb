package de.whitehatracoons.todoList.service.user;

import de.whitehatracoons.todoList.model.task.Task;
import de.whitehatracoons.todoList.model.user.User;
import de.whitehatracoons.todoList.model.user.UserCredentials;

import java.util.List;

/**
 * Created by venance on 24.06.17.
 */
public interface UserService {
    List<User> findAll();
    User findUserById(String userId);
    String storeUser(User user);
    String addTaskToUser(String userId, Task testTask);
    List<Task> getAllTasksForUser(String userId);
    boolean updateUserTaskState(String userId, String taskKey, boolean state);
    boolean deleteUserTask(String userId, String taskKey);
    boolean deleteUser(String userID);

    String handleLogin(UserCredentials userCredentials);
}
