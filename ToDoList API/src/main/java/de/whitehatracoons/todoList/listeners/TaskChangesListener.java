package de.whitehatracoons.todoList.listeners;

import com.rethinkdb.RethinkDB;
import com.rethinkdb.net.Cursor;
import de.whitehatracoons.todoList.database.RethinkDBConnectionFactory;
import de.whitehatracoons.todoList.model.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Created by venance on 11.07.17.
 */
@Service
public class TaskChangesListener {

    private static final RethinkDB r = RethinkDB.r;

    private RethinkDBConnectionFactory connectionFactory;

    private SimpMessagingTemplate webSocket;

    @Value("${whitehatracoons.application.database}")
    private String database;

    @Autowired
    public TaskChangesListener(RethinkDBConnectionFactory rethinkDBConnectionFactory, SimpMessagingTemplate template) {
        this.connectionFactory = rethinkDBConnectionFactory;
        this.webSocket = template;
    }

    @Async
    public void pushChangesToWebSocket(String userId) {
        Cursor cursor = r.db(this.database).table("users").get(userId).changes().map(
                val -> val.g("new_val").g("tasks").coerceTo("array").difference(
                        val.g("old_val").g("tasks").coerceTo("array")
                ).coerceTo("object")
        ).run(connectionFactory.createConnection());

        while (cursor.hasNext()) {
            Map<String, Object> doc = (Map<String, Object>) cursor.next();
            if (!doc.isEmpty()) {
                Task task = new Task();
                for (Object o : doc.values()) {
                    Map<String, Object> taskDoc = (Map<String, Object>) o;
                    task.setId((String) taskDoc.get("id"));
                    task.setTitle((String) taskDoc.get("title"));
                    task.setText((String) taskDoc.get("text"));
                    task.setDone((Boolean) taskDoc.get("done"));
                }
                webSocket.convertAndSend("/tasks/" + userId, task);
            }
        }
    }
}
