package de.whitehatracoons.todoList.database;

import com.rethinkdb.RethinkDB;
import com.rethinkdb.net.Connection;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by venance on 24.06.17.
 */
@Component
public class RethinkDBConnectionFactory {
    @Value("${whitehatracoons.application.host}")
    private String host;

    public RethinkDBConnectionFactory() {
    }

    public Connection createConnection() {
        return RethinkDB.r.connection().hostname(host).port(28015).connect();
    }
}
