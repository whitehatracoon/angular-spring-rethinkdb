package de.whitehatracoons.todoList.database;

import com.rethinkdb.RethinkDB;
import com.rethinkdb.net.Connection;
import com.rethinkdb.net.Cursor;
import de.whitehatracoons.todoList.entity.user.UserEntity;
import de.whitehatracoons.todoList.exception.task.TaskNotAddedException;
import de.whitehatracoons.todoList.exception.task.TaskNotFoundException;
import de.whitehatracoons.todoList.exception.task.TaskStateNotUpdatedException;
import de.whitehatracoons.todoList.exception.user.UserCouldNotBeDeletedException;
import de.whitehatracoons.todoList.exception.user.UserNotFoundException;
import de.whitehatracoons.todoList.model.task.Task;
import de.whitehatracoons.todoList.model.user.UserCredentials;
import de.whitehatracoons.todoList.util.TaskKeyGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by venance on 24.06.17.
 */
@Repository
@Scope("singleton")
public class DatabaseConnector {

    private RethinkDBConnectionFactory connectionFactory;
    private static final RethinkDB r = RethinkDB.r;
    private static String DATABASE;
    private static final String USERS_TABLE = "users";

    @Autowired
    public DatabaseConnector(@Value("${whitehatracoons.application.database}") String database, RethinkDBConnectionFactory rethinkDBConnectionFactory) {
        this.connectionFactory = rethinkDBConnectionFactory;
        DATABASE = database;
    }

    public String storeUser(UserEntity userEntity) {
        Connection connection = connectionFactory.createConnection();
        Map<String, Object> result = r.db(DATABASE).table("users").insert(
                r.hashMap()
                    .with("userName", userEntity.getUserName())
                    .with("email", userEntity.getEmail())
                    .with("password", userEntity.getPassword())
                    .with("tasks", r.hashMap())
        ).run(connection);
        connection.close();
        List<String> keys = (List<String>) result.get("generated_keys");
        return keys.get(0);
    }

    public UserEntity findUserById(String userId) {
        Connection connection = connectionFactory.createConnection();
        Map<String, Object> userDoc = r.db(DATABASE).table("users").get(userId).run(connection);
        UserEntity userEntity = this.mapToUserEntity(userDoc);
        connection.close();
        return userEntity;
    }

    public List<UserEntity> findAllUsers() {
        Connection connection = connectionFactory.createConnection();
        Cursor cursor = r.db(DATABASE).table("users").run(connection);
        List<UserEntity> entities = new ArrayList<>();
        connection.close();
        for (Object doc : cursor) {
           entities.add(mapToUserEntity(doc));
        }
        return entities;
    }

    public String addTaskToUser(String userId, Task task) {
        TaskKeyGenerator keyGenerator = new TaskKeyGenerator();
        String taskKey = keyGenerator.generateTaskKey();
        task.setId(taskKey);
        Connection connection = connectionFactory.createConnection();
        Map<String, Object> result = r.db(DATABASE).table("users").get(userId).update(
                row -> r.hashMap("tasks", row.g("tasks").merge(r.object(task.getId(), task)))
        ).run(connection);
        connection.close();
        if ( (long) result.get("replaced") != 1 || (long) result.get("skipped") == 1 ) throw new TaskNotAddedException("Task could not be added to user with id: " + userId);
        return task.getId();
    }

    public List<Task> findAllTasksForUser(String userId) {
        List<Task> tasks = new ArrayList<>();
        Connection connection = connectionFactory.createConnection();
        Map<String, Object> savedTasks = r.db(DATABASE).table("users").get(userId).g("tasks").run(connection);
        for ( Object taskObj : savedTasks.values() ) {
            Map<String, Object> tasEntry = (Map<String, Object>) taskObj;
            Task task = new Task();
            task.setId((String) tasEntry.get("id"));
            task.setTitle((String) tasEntry.get("title"));
            task.setText((String) tasEntry.get("text"));
            task.setDone((boolean) tasEntry.get("done"));
            tasks.add(task);
        }
        connection.close();
        return tasks;
    }

    public boolean updateUserTaskState(String id, String taskKey, boolean state) {
        Connection connection = connectionFactory.createConnection();
        Map<String, Object> result = r.db(DATABASE).table("users").get(id).update(
                r.hashMap("tasks",
                        r.hashMap(taskKey,
                                r.hashMap("done", state)))
        ).run(connection);
        connection.close();
        if ( (long) result.get("unchanged") == 1) throw new TaskStateNotUpdatedException("task state was already " + state);
        if ( (long) result.get("replaced") == 1 ) return true;
        return false;
    }

    public boolean deleteUserTask(String userId, String taskKey) {
        Connection connection = connectionFactory.createConnection();
        Map<String, Object> result = r.db(DATABASE).table(USERS_TABLE).get(userId).replace(
                row -> row.without(r.hashMap("tasks", r.hashMap(taskKey, true)))
        ).run(connection);
        connection.close();
        if ( (long) result.get("unchanged") == 1) throw new TaskNotFoundException("the task with the key: " + taskKey + " was not found");
        if ( (long) result.get("replaced") == 1 ) return true;
        return false;
    }

    public boolean deleteUser(String userKey) {
        Connection connection = connectionFactory.createConnection();
        Map<String, Object> result = r.db(DATABASE).table("users").get(userKey).delete().run(connection);
        connection.close();
        if ( (long) result.get("skipped") == 1 ) throw new UserCouldNotBeDeletedException();
        if ( (long) result.get("deleted") == 1 ) return true;
        return false;
    }

    private UserEntity mapToUserEntity(Object doc) {
        Map<String, Object> entry = (Map<String, Object>) doc;
        UserEntity user = new UserEntity();
        user.setUserId((String) entry.get("userId"));
        user.setUserName((String) entry.get("userName"));
        user.setEmail((String) entry.get("email"));
        user.setPassword((String) entry.get("password"));
        Map<String, Object> tasksEntryDoc = (Map<String, Object>) entry.get("tasks");
        if ( tasksEntryDoc != null ) {
            Map<String, Task> todos = new HashMap<>();
            for ( Object taskDoc : tasksEntryDoc.values() ) {
                Map<String, Object> taskEntry = (Map<String, Object>) taskDoc;
                Task task = new Task((String )taskEntry.get("title"), (String) taskEntry.get("text"));
                task.setId((String) taskEntry.get("id"));
                task.setDone((boolean) taskEntry.get("done"));
                todos.put(task.getId(), task);
            }
            user.setTasks(todos);
        }
        return user;
    }

    public Map<String, String> returnUserToVerify(UserCredentials userCredentials) {
        Connection connection = connectionFactory.createConnection();
        Cursor cursor = r.db(DATABASE).table("users").filter(r.hashMap("userName", userCredentials.getUserName())).withFields("id", "userName", "password").run(connection);
        connection.close();
        if (!cursor.hasNext()) throw new UserNotFoundException("user with user name: " + userCredentials.getUserName() + "could not be found.");
        Map<String, String> user = null;
        for (Object doc : cursor) {
            user = (Map<String, String>) doc;
        }
        return user;
    }
}
