package de.whitehatracoons.todoList.entity.user;

import de.whitehatracoons.todoList.model.task.Task;

import java.io.Serializable;
import java.util.Map;

/**
 * Created by venance on 24.06.17.
 */
public class UserEntity implements Serializable {
    private String userId;
    private String userName;
    private String email;
    private String password;
    private Map<String, Task> tasks;

    public UserEntity() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Map<String, Task> getTasks() {
        return tasks;
    }

    public void setTasks(Map<String, Task> tasks) {
        this.tasks = tasks;
    }
}
