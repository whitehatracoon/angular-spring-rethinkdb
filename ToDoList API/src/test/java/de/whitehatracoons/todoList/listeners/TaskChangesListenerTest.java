package de.whitehatracoons.todoList.listeners;

import de.whitehatracoons.todoList.Application;
import de.whitehatracoons.todoList.database.DatabaseConnector;
import de.whitehatracoons.todoList.entity.user.UserEntity;
import de.whitehatracoons.todoList.model.task.Task;
import de.whitehatracoons.todoList.util.Mapper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.Transport;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;

import static Util.TestHelper.getTestTask;
import static Util.TestHelper.getTestUser;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by venance on 11.07.17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT, properties = {"server.port=8089"})
@ActiveProfiles("test")
public class TaskChangesListenerTest {
    @Value("${whitehatracoons.application.database}")
    private String database;

    @Autowired
    private DatabaseConnector databaseConnector;

    private static String WEBSOCKET_URI = "ws://localhost:8089/taskWS";
    private static final String WEBSOCKET_TOPIC = "/tasks/";

    private BlockingQueue<Task> blockingQueue;

    @Autowired
    private TaskChangesListener taskChangesListener;

    @Autowired
    private Mapper mapper;

    @Before
    public void setup() {
        blockingQueue = new LinkedBlockingDeque<>();
    }

    @After
    public void tearDown() {
        blockingQueue = null;
    }

    @Test
    public void pushChangesToWebSocketAfterAddingATaskToUser() throws Exception {
        String userId = createTestUserAndReturnId();
        taskChangesListener.pushChangesToWebSocket(userId);
        WebSocketStompClient stompClient = new WebSocketStompClient(new SockJsClient(createTransportClient()));
        stompClient.setMessageConverter(new MappingJackson2MessageConverter());
        StompSession session = stompClient.connect(WEBSOCKET_URI, new StompSessionHandlerAdapter() {}).get();
        session.subscribe(WEBSOCKET_TOPIC + userId, new TaskStompFrameHandler());

        Task expected = getTestTask();
        databaseConnector.addTaskToUser(userId, expected);

        Task actual = blockingQueue.poll(1, TimeUnit.SECONDS);
        assertThat(actual, notNullValue());
        assertThat(actual.getTitle(), is(expected.getTitle()));
        assertThat(actual.getText(), is(expected.getText()));
        assertThat(actual.getDone(), is(expected.getDone()));
    }

    @Test
    public void pushChangesToWebSocketAfterUpdatingATaskState() throws Exception {
        String userId = createTestUserAndReturnId();
        Task expected = getTestTask();
        String taskKey = databaseConnector.addTaskToUser(userId, expected);
        taskChangesListener.pushChangesToWebSocket(userId);
        WebSocketStompClient stompClient = new WebSocketStompClient(new SockJsClient(createTransportClient()));
        stompClient.setMessageConverter(new MappingJackson2MessageConverter());
        StompSession session = stompClient.connect(WEBSOCKET_URI, new StompSessionHandlerAdapter() {}).get();
        session.subscribe(WEBSOCKET_TOPIC + userId, new TaskStompFrameHandler());

        databaseConnector.updateUserTaskState(userId, taskKey, true);

        Task actual = blockingQueue.poll(1, TimeUnit.SECONDS);
        assertThat(actual, notNullValue());
        assertThat(actual.getTitle(), is(expected.getTitle()));
        assertThat(actual.getText(), is(expected.getText()));
        assertThat(actual.getDone(), not(is(expected.getDone())));
    }

    @Test
    public void pushNoChangesToWebSocketAfterDeletingATask() throws Exception {
        String userId = createTestUserAndReturnId();
        Task expected = getTestTask();
        String taskKey = databaseConnector.addTaskToUser(userId, expected);
        taskChangesListener.pushChangesToWebSocket(userId);
        WebSocketStompClient stompClient = new WebSocketStompClient(new SockJsClient(createTransportClient()));
        stompClient.setMessageConverter(new MappingJackson2MessageConverter());
        StompSession session = stompClient.connect(WEBSOCKET_URI, new StompSessionHandlerAdapter() {}).get();
        session.subscribe(WEBSOCKET_TOPIC + userId, new TaskStompFrameHandler());

        databaseConnector.deleteUserTask(userId, taskKey);

        Task actual = blockingQueue.poll(1, TimeUnit.SECONDS);
        assertThat(actual, nullValue());
    }

    private String createTestUserAndReturnId() {
        return databaseConnector.storeUser(mapper.map(getTestUser(), UserEntity.class));
    }

    private List<Transport> createTransportClient() {
        List<Transport> transports = new ArrayList<>(1);
        transports.add(new WebSocketTransport(new StandardWebSocketClient()));
        return transports;
    }

    private class TaskStompFrameHandler implements StompFrameHandler {
        @Override
        public Type getPayloadType(StompHeaders stompHeaders) {
            return Task.class;
        }

        @Override
        public void handleFrame(StompHeaders stompHeaders, Object o) {
            blockingQueue.offer((Task) o);
        }
    }
}