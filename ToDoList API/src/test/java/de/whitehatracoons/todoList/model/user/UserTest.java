package de.whitehatracoons.todoList.model.user;

import de.whitehatracoons.todoList.model.task.Task;
import org.hamcrest.collection.IsMapContaining;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

/**
 * Created by venance on 24.06.17.
 */
public class UserTest {
    private User user;

    @Before
    public void setUp() throws Exception {
        user = new User("Jaro", "jaro@whitehatracoons.de", "secret");
    }

    @After
    public void tearDown() throws Exception {
        user = null;
    }

    @Test
    public void addTodo() throws Exception {
        Task task = new Task("add task", "test to add a task");

        String taskKey = user.addTask(task);

        Map<String, Task> actual = user.getTasks();
        assertThat(actual, notNullValue());
        assertThat(actual.size(), is(1));
        assertThat(actual, IsMapContaining.hasKey(taskKey));
        assertThat(actual, IsMapContaining.hasValue(task));
        assertThat(actual, IsMapContaining.hasEntry(taskKey, task));
    }

    @Test
    public void removeTodo() throws Exception {
        Task task = new Task("remove task", "test to remove a task");
        String taskKey = user.addTask(task);

        user.removeTask(taskKey);

        Map<String, Task> actual = user.getTasks();
        assertThat(actual, notNullValue());
        assertThat(actual.size(), is(0));
        assertThat(actual, not(IsMapContaining.hasKey(taskKey)));
        assertThat(actual, not(IsMapContaining.hasValue(task)));
        assertThat(actual, not(IsMapContaining.hasEntry(taskKey, task)));
    }

}