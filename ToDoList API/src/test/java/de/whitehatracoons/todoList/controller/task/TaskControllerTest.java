package de.whitehatracoons.todoList.controller.task;

import Util.TestHelper;
import de.whitehatracoons.todoList.Application;
import de.whitehatracoons.todoList.database.DatabaseConnector;
import de.whitehatracoons.todoList.database.RethinkDBConnectionFactory;
import de.whitehatracoons.todoList.entity.user.UserEntity;
import de.whitehatracoons.todoList.model.state.State;
import de.whitehatracoons.todoList.model.task.Task;
import de.whitehatracoons.todoList.util.Mapper;
import org.hamcrest.collection.IsEmptyCollection;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static Util.ContentTypeEnum.contentType_JSON_utf8;
import static Util.ContentTypeEnum.contentType_TextPlain_utf8;
import static Util.TestHelper.getTestTask;
import static Util.TestHelper.getTestUser;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

/**
 * Created by venance on 10.07.17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
@ActiveProfiles("test")
public class TaskControllerTest {

    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext webApplicationContext;
    private HttpMessageConverter mappingJackson2HttpMessageConverter;
    @Value("${whitehatracoons.application.database}")
    private String database;
    @Autowired
    private RethinkDBConnectionFactory connectionFactory;
    @Autowired
    private DatabaseConnector databaseConnector;
    private static final String USERS_PATH = "/users";
    @Autowired
    private Mapper mapper;

    @Before
    public void setUp() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @After
    public void tearDown() throws Exception {
        TestHelper.cleanDatabaseTable(this.database, "users", connectionFactory);
    }

    @Test
    public void addTask() throws Exception {
        String userId = createTestUserAndReturnId();
        String taskJson = this.json(getTestTask());

        this.mockMvc.perform(post(USERS_PATH+"/"+userId+"/tasks")
                .contentType(contentType_JSON_utf8)
                .content(taskJson))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(contentType_TextPlain_utf8));
    }
    @Test
    public void addTaskValidationEmptyTitle() throws Exception {
        String userId = createTestUserAndReturnId();
        Task task = new Task();
        task.setText("test task");
        task.setDone(false);
        String taskJson = this.json(task);

        this.mockMvc.perform(post(USERS_PATH+"/"+userId+"/tasks")
                .contentType(contentType_JSON_utf8)
                .content(taskJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void getAllTasks() throws Exception {
        String userId = databaseConnector.storeUser(mapper.map(getTestUser(), UserEntity.class));
        Task task1 = getTestTask();
        Task task2 = getTestTask();
        Task task3 = getTestTask();
        databaseConnector.addTaskToUser(userId, task1);
        databaseConnector.addTaskToUser(userId, task2);
        databaseConnector.addTaskToUser(userId, task3);

        this.mockMvc.perform(get(USERS_PATH+"/"+userId+"/tasks"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType_JSON_utf8))
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[0].title", is(task1.getTitle())))
                .andExpect(jsonPath("$[1].title", is(task2.getTitle())))
                .andExpect(jsonPath("$[2].title", is(task3.getTitle())))
                .andExpect(jsonPath("$[0].text", is(task1.getText())))
                .andExpect(jsonPath("$[1].text", is(task2.getText())))
                .andExpect(jsonPath("$[2].text", is(task3.getText())));
    }

    @Test
    public void updateTaskState() throws Exception {
        String userId = databaseConnector.storeUser(mapper.map(getTestUser(), UserEntity.class));
        Task expected = getTestTask();
        String taskKey = databaseConnector.addTaskToUser(userId, expected);
        State state = new State();
        state.setValue(true);
        String stateJson = json(state);

        this.mockMvc.perform(patch(USERS_PATH+"/"+userId+"/tasks/"+taskKey)
                .contentType(contentType_JSON_utf8)
                .content(stateJson))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType_JSON_utf8))
                .andExpect(jsonPath("$", is(true)));

        Task actual = databaseConnector.findUserById(userId).getTasks().get(taskKey);
        assertThat(actual.getTitle(), is(expected.getTitle()));
        assertThat(actual.getText(), is(expected.getText()));
        assertThat(actual.getDone(), not(is(expected.getDone())));
        assertThat(actual.getDone(), is(true));
    }

    @Test
    public void updateTaskStateFails() throws Exception {
        String userId = databaseConnector.storeUser(mapper.map(getTestUser(), UserEntity.class));
        Task expected = getTestTask();
        String taskKey = databaseConnector.addTaskToUser(userId, expected);
        State state = new State();
        state.setValue(false);
        String stateJson = json(state);

        this.mockMvc.perform(patch(USERS_PATH+"/"+userId+"/tasks/"+taskKey)
                .contentType(contentType_JSON_utf8)
                .content(stateJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void deleteTask() throws Exception {
        String userId = databaseConnector.storeUser(mapper.map(getTestUser(), UserEntity.class));
        String taskKey = databaseConnector.addTaskToUser(userId, getTestTask());

        this.mockMvc.perform(delete(USERS_PATH+"/"+userId+"/tasks/"+taskKey))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", is(true)));

        List<Task> tasks = databaseConnector.findAllTasksForUser(userId);
        assertThat(tasks, notNullValue());
        assertThat(tasks, is(IsEmptyCollection.empty()));
    }

    @Test
    public void deleteTaskTaskNotFound() throws Exception {
        String userId = databaseConnector.storeUser(mapper.map(getTestUser(), UserEntity.class));

        this.mockMvc.perform(delete(USERS_PATH+"/"+userId+"/tasks/falseKey"))
                .andExpect(status().isNotFound());
    }

    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

        mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
                .filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter)
                .findAny()
                .orElse(null);

        assertNotNull("the JSON message converter must not be null",
                mappingJackson2HttpMessageConverter);
    }

    private String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
                o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }

    private String createTestUserAndReturnId() {
        return databaseConnector.storeUser(mapper.map(getTestUser(), UserEntity.class));
    }

}