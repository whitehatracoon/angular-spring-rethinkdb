package de.whitehatracoons.todoList.controller.user;

import Util.TestHelper;
import de.whitehatracoons.todoList.Application;
import de.whitehatracoons.todoList.database.DatabaseConnector;
import de.whitehatracoons.todoList.database.RethinkDBConnectionFactory;
import de.whitehatracoons.todoList.entity.user.UserEntity;
import de.whitehatracoons.todoList.model.user.User;
import de.whitehatracoons.todoList.model.user.UserCredentials;
import de.whitehatracoons.todoList.util.Mapper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.util.Arrays;

import static Util.ContentTypeEnum.contentType_JSON_utf8;
import static Util.ContentTypeEnum.contentType_TextPlain_utf8;
import static Util.TestHelper.getTestTask;
import static Util.TestHelper.getTestUser;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

/**
 * Created by venance on 25.06.17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
@ActiveProfiles("test")
public class UserControllerTest {

    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext webApplicationContext;
    private HttpMessageConverter mappingJackson2HttpMessageConverter;
    @Value("${whitehatracoons.application.database}")
    private String database;
    @Autowired
    private RethinkDBConnectionFactory connectionFactory;
    @Autowired
    private DatabaseConnector databaseConnector;
    private static final String USERS_PATH = "/users";
    @Autowired
    private Mapper mapper;

    @Before
    public void setUp() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @After
    public void tearDown() throws Exception {
        TestHelper.cleanDatabaseTable(this.database, "users", connectionFactory);
    }

    @Test
    public void registerUser() throws Exception {
        User user = new User("Jaro", "jaro@whitehatracoons.de", "secret");
        String userJson = this.json(user);
        this.mockMvc.perform(post(USERS_PATH+"/register")
                                .contentType(contentType_JSON_utf8)
                                .content(userJson))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(contentType_TextPlain_utf8));
    }

    @Test
    public void registerUserValidationNameEmailFormat() throws Exception {
        User user = new User("Jaro", "jaroATwhitehatracoons.de", "secret");
        String userJson = this.json(user);
        this.mockMvc.perform(post(USERS_PATH+"/register")
                .contentType(contentType_JSON_utf8)
                .content(userJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void registerUserValidationTasksNotEmpty() throws Exception {
        User user = new User("Jaro", "jaro@whitehatracoons.de", "secret");
        user.addTask(getTestTask());
        String userJson = this.json(user);
        this.mockMvc.perform(post(USERS_PATH+"/register")
                                .contentType(contentType_JSON_utf8)
                                .content(userJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void getUser() throws Exception {
        User expected = getTestUser();
        String userId = databaseConnector.storeUser(mapper.map(expected, UserEntity.class));

        this.mockMvc.perform(get(USERS_PATH+"/"+userId))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType_JSON_utf8))
                .andExpect(jsonPath("$.userName", is(expected.getUserName())))
                .andExpect(jsonPath("$.email", is(expected.getEmail())));
    }

    @Test
    public void unregisterUser() throws Exception {
        String userId = createTestUserAndReturnId();

        this.mockMvc.perform(delete(USERS_PATH+"/"+userId))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType_JSON_utf8))
                .andExpect(jsonPath("$", is(true)));
    }

    @Test
    public void login() throws Exception {
        User user = getTestUser();
        String userId =  databaseConnector.storeUser(mapper.map(user, UserEntity.class));
        UserCredentials userCredentials = new UserCredentials(user.getUserName(), user.getPassword());

        this.mockMvc.perform(post(USERS_PATH+"/login")
                                .contentType(contentType_JSON_utf8)
                                .content(json(userCredentials)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType_TextPlain_utf8))
                .andExpect(content().string(is(userId)));
    }

    @Test
    public void loginFails() throws Exception {
        User user = getTestUser();
        databaseConnector.storeUser(mapper.map(user, UserEntity.class));
        UserCredentials userCredentials = new UserCredentials(user.getUserName(), "wrong pass");

        this.mockMvc.perform(post(USERS_PATH+"/login")
                .contentType(contentType_JSON_utf8)
                .content(json(userCredentials)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void loginUserNotFound() throws Exception {
        UserCredentials userCredentials = new UserCredentials("not existing user", "any pass");

        this.mockMvc.perform(post(USERS_PATH+"/login")
                .contentType(contentType_JSON_utf8)
                .content(json(userCredentials)))
                .andExpect(status().isNotFound());
    }

    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

        mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
                .filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter)
                .findAny()
                .orElse(null);

        assertNotNull("the JSON message converter must not be null",
                mappingJackson2HttpMessageConverter);
    }

    private String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
                o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }

    private String createTestUserAndReturnId() {
        return databaseConnector.storeUser(mapper.map(getTestUser(), UserEntity.class));
    }

}