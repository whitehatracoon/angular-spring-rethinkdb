package de.whitehatracoons.todoList.service.user;

import Util.TestHelper;
import de.whitehatracoons.todoList.Application;
import de.whitehatracoons.todoList.database.RethinkDBConnectionFactory;
import de.whitehatracoons.todoList.exception.task.TaskNotFoundException;
import de.whitehatracoons.todoList.exception.task.TaskStateNotUpdatedException;
import de.whitehatracoons.todoList.exception.user.UserNotFoundException;
import de.whitehatracoons.todoList.model.task.Task;
import de.whitehatracoons.todoList.model.user.User;
import de.whitehatracoons.todoList.model.user.UserCredentials;
import org.hamcrest.collection.IsEmptyCollection;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static Util.TestHelper.getTestTask;
import static Util.TestHelper.getTestUser;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

/**
 * Created by venance on 25.06.17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@ActiveProfiles("test")
public class UserServiceTest {
    @Autowired
    private UserService userService;
    @Value("${whitehatracoons.application.database}")
    private String database;
    @Autowired
    private RethinkDBConnectionFactory connectionFactory;

    @After
    public void tearDown() {
        TestHelper.cleanDatabaseTable(this.database, "users", connectionFactory);
    }

    @Test
    public void findAll() throws Exception {
        User user1 = TestHelper.getTestUser();
        User user2 = new User();
        user2.setUserName("Sora");
        user2.setPassword("sora's secret");
        user2.setEmail("sora@whitehatracoons.de");
        userService.storeUser(user1);
        userService.storeUser(user2);

        List<User> actual = userService.findAll();

        assertThat(actual, notNullValue());
        assertThat(actual, not(IsEmptyCollection.empty()));
        assertThat(new ArrayList<>(), IsEmptyCollection.empty());
        assertThat(actual.size(), is(2));
    }

    @Test
    public void findUserById() throws Exception {
        User user = getTestUser();
        String userId = userService.storeUser(user);

        User actual = userService.findUserById(userId);

        assertThat(actual, notNullValue());
        assertThat(actual.getUserName(), is(user.getUserName()));
        assertThat(actual.getEmail(), is(user.getEmail()));
        assertThat(actual.getPassword(), is(user.getPassword()));
    }

    @Test
    public void addTaskToUser() throws Exception {
        String userId = userService.storeUser(getTestUser());
        Task task = getTestTask();

        String taskKey = userService.addTaskToUser(userId, task);

        Task actual = userService.findUserById(userId).getTasks().get(taskKey);
        assertThat(actual, notNullValue());
        assertThat(actual.getTitle(), is(task.getTitle()));
        assertThat(actual.getText(), is(task.getText()));
    }

    @Test
    public void getAllTasksForUser() throws Exception {
        String userId = userService.storeUser(getTestUser());
        userService.addTaskToUser(userId, getTestTask());
        userService.addTaskToUser(userId, getTestTask());

        List<Task> actual = userService.getAllTasksForUser(userId);

        assertThat(actual, notNullValue());
        assertThat(actual.size(), is(2));
    }

    @Test
    public void updateUserTaskState() throws Exception {
        String userId = userService.storeUser(getTestUser());
        String taskKey = userService.addTaskToUser(userId, getTestTask());

        boolean updated = userService.updateUserTaskState(userId, taskKey, true);

        Task actual = userService.findUserById(userId).getTasks().get(taskKey);
        assertThat(updated, is(true));
        assertThat(actual.getDone(), is(true));
    }

    @Test(expected = TaskStateNotUpdatedException.class)
    public void updateUserTaskStateFails() throws Exception {
        String userId = userService.storeUser(getTestUser());
        String taskKey = userService.addTaskToUser(userId, getTestTask());

        userService.updateUserTaskState(userId, taskKey, false);
    }

    @Test
    public void deleteUserTask() throws Exception {
        String userId = userService.storeUser(getTestUser());
        String taskKey = userService.addTaskToUser(userId, getTestTask());

        boolean deleted = userService.deleteUserTask(userId, taskKey);

        List<Task> tasks = userService.getAllTasksForUser(userId);
        assertThat(deleted, is(true));
        assertThat(tasks, notNullValue());
        assertThat(tasks, is(IsEmptyCollection.empty()));
    }

    @Test(expected = TaskNotFoundException.class)
    public void deleteUserTaskTaskNotFound() throws Exception {
        String userId = userService.storeUser(getTestUser());

        userService.deleteUserTask(userId, "not existing task key");
    }

    @Test
    public void deleteUser() throws Exception {
        String userId = userService.storeUser(getTestUser());

        boolean deleted = userService.deleteUser(userId);

        assertThat(deleted, is(true));
    }

    @Test
    public void handleLogin() throws Exception {
        User user = getTestUser();
        String userId = userService.storeUser(user);
        UserCredentials userCredentials = new UserCredentials(user.getUserName(), user.getPassword());

        String actual = userService.handleLogin(userCredentials);

        assertThat(actual, notNullValue());
        assertThat(actual, is(userId));
    }

    @Test
    public void handleLoginFails() throws Exception {
        User user = getTestUser();
        userService.storeUser(user);
        UserCredentials userCredentials = new UserCredentials(user.getUserName(), "wrong pass");

        String actual = userService.handleLogin(userCredentials);

        assertThat(actual, nullValue());
    }

    @Test(expected = UserNotFoundException.class)
    public void handleLoginUserNotFound() throws Exception {
        UserCredentials userCredentials = new UserCredentials("not existing user", "any pass");

        userService.handleLogin(userCredentials);
    }

}