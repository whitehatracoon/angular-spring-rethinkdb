package de.whitehatracoons.todoList.database;

import Util.TestHelper;
import de.whitehatracoons.todoList.Application;
import de.whitehatracoons.todoList.entity.user.UserEntity;
import de.whitehatracoons.todoList.exception.task.TaskNotAddedException;
import de.whitehatracoons.todoList.exception.task.TaskNotFoundException;
import de.whitehatracoons.todoList.exception.task.TaskStateNotUpdatedException;
import de.whitehatracoons.todoList.exception.user.UserCouldNotBeDeletedException;
import de.whitehatracoons.todoList.exception.user.UserNotFoundException;
import de.whitehatracoons.todoList.model.task.Task;
import de.whitehatracoons.todoList.model.user.UserCredentials;
import org.hamcrest.collection.IsEmptyCollection;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static Util.TestHelper.getTestTask;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Created by venance on 24.06.17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@ActiveProfiles("test")
public class DatabaseConnectorTest {

    @Autowired
    private DatabaseConnector databaseConnector;
    @Autowired
    private RethinkDBConnectionFactory connectionFactory;
    private static final String DATABASE = "todoListTestDb";

    @After
    public void tearDown() {
        TestHelper.cleanDatabaseTable(DATABASE, "users", connectionFactory);
    }

    @Test
    public void findUserById() {
        UserEntity user = getTestUser();
        String userId = databaseConnector.storeUser(user);

        UserEntity actual = databaseConnector.findUserById(userId);

        assertThat(actual, notNullValue());
        assertThat(actual.getUserName(), is(user.getUserName()));
        assertThat(actual.getEmail(), is(user.getEmail()));
        assertThat(actual.getPassword(), is(user.getPassword()));
    }

    @Test
    public void findAllUsers() throws Exception {
        UserEntity user = new UserEntity();
        user.setUserName("Sora");
        user.setPassword("sora's secret");
        user.setEmail("sora@whitehatracoons.de");
        databaseConnector.storeUser(getTestUser());
        databaseConnector.storeUser(user);

        List<UserEntity> actual = databaseConnector.findAllUsers();

        assertThat(actual, notNullValue());

        assertThat(actual, not(IsEmptyCollection.empty()));
        assertThat(new ArrayList<>(), IsEmptyCollection.empty());
        assertThat(actual.size(), is(2));
    }

    @Test
    public void addTaskToUser() throws Exception {
        Task task = getTestTask();
        String userId = databaseConnector.storeUser(getTestUser());

        String taskKey = databaseConnector.addTaskToUser(userId, task);
        Task actualTask = databaseConnector.findUserById(userId).getTasks().get(taskKey);

        assertThat(taskKey, notNullValue());
        assertThat(taskKey.length(), is(10));
        assertTrue(taskKey.getClass().equals(String.class));
        assertThat(actualTask.getTitle(), is(task.getTitle()));
        assertThat(actualTask.getText(), is(task.getText()));
        assertThat(actualTask.getDone(), is(false));
        assertThat(actualTask.getDone(), is(task.getDone()));
    }

    @Test(expected = TaskNotAddedException.class)
    public void addTaskToUserFails() throws Exception {
        databaseConnector.addTaskToUser("not existing id", getTestTask());
    }

    @Test
    public void findAllTasksForUser() throws Exception {
        UserEntity user = getTestUser();
        String userId = databaseConnector.storeUser(user);
        Task task = getTestTask();
        databaseConnector.addTaskToUser(userId, task);

        List<Task> actual = databaseConnector.findAllTasksForUser(userId);

        assertThat(actual, notNullValue());
        assertThat(actual.size(), is(1));
        assertThat(actual.get(0).getTitle(), is(task.getTitle()));
        assertThat(actual.get(0).getText(), is(task.getText()));
    }

    @Test
    public void updateUserTaskState() throws Exception {
        String userId = databaseConnector.storeUser(getTestUser());
        Task task = getTestTask();
        String taskKey = databaseConnector.addTaskToUser(userId, task);

        boolean updated = databaseConnector.updateUserTaskState(userId, taskKey, true);

        Task actual = databaseConnector.findUserById(userId).getTasks().get(taskKey);
        assertThat(updated, is(true));
        assertThat(actual.getTitle(), is(task.getTitle()));
        assertThat(actual.getText(), is(task.getText()));
        assertThat(actual.getDone(), is(true));
    }

    @Test(expected = TaskStateNotUpdatedException.class)
    public void updateUserTaskStateFails() throws Exception {
        UserEntity user = getTestUser();
        String userId = databaseConnector.storeUser(user);
        String taskKey = databaseConnector.addTaskToUser(userId, getTestTask());

        databaseConnector.updateUserTaskState(userId, taskKey, false);
    }

    @Test
    public void deleteUserTask() throws Exception {
        UserEntity user = getTestUser();
        String userId = databaseConnector.storeUser(user);
        String taskKey = databaseConnector.addTaskToUser(userId, getTestTask());

        boolean deleted = databaseConnector.deleteUserTask(userId, taskKey);

        List<Task> tasks = databaseConnector.findAllTasksForUser(userId);
        assertThat(deleted, is(true));
        assertThat(tasks, notNullValue());
        assertThat(tasks, is(IsEmptyCollection.empty()));
    }

    @Test(expected = TaskNotFoundException.class)
    public void deleteUserTaskTaskNotFound() throws Exception {
        UserEntity user = getTestUser();
        String userId = databaseConnector.storeUser(user);

        databaseConnector.deleteUserTask(userId, "not existing task key");
    }

    @Test
    public void deleteUser() throws Exception {
        String userId = databaseConnector.storeUser(getTestUser());

        boolean deleted = databaseConnector.deleteUser(userId);

        assertThat(deleted, is(true));
    }

    @Test(expected = UserCouldNotBeDeletedException.class)
    public void deleteUserFails() throws Exception {
        databaseConnector.deleteUser("this is a not existing user key");
    }

    @Test
    public void returnUserToVerify() throws Exception {
        UserEntity user = getTestUser();
        String userId = databaseConnector.storeUser(user);
        UserCredentials userCredentials = new UserCredentials(user.getUserName(), user.getPassword());

        Map<String, String> actual =  databaseConnector.returnUserToVerify(userCredentials);

        assertThat(actual, notNullValue());
        assertThat(actual.get("userName"), is(user.getUserName()));
        assertThat(actual.get("password"), is(user.getPassword()));
        assertThat(actual.get("id"), is(userId));
    }

    @Test(expected = UserNotFoundException.class)
    public void returnUserToVerifyDoesNotFindUser() throws Exception {
        UserCredentials userCredentials = new UserCredentials("not existing user", "pass");

        databaseConnector.returnUserToVerify(userCredentials);
    }

    private UserEntity getTestUser() {
        UserEntity user = new UserEntity();
        user.setUserName("Jaro");
        user.setEmail("jaro@whitehatracoons.de");
        user.setPassword("secret");
        return user;
    }

}