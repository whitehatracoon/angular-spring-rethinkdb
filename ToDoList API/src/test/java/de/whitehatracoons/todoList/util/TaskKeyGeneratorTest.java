package de.whitehatracoons.todoList.util;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

/**
 * Created by venance on 27.06.17.
 */
public class TaskKeyGeneratorTest {
    private TaskKeyGenerator keyGenerator;

    @Before
    public void setUp() throws Exception {
        keyGenerator = new TaskKeyGenerator();
    }

    @After
    public void tearDown() throws Exception {
        keyGenerator = null;
    }

    @Test
    public void generateTaskKey() throws Exception {
        String actual = keyGenerator.generateTaskKey();

        assertThat(actual, notNullValue());
        assertThat(actual.isEmpty(), is(false));
        assertThat(actual.length(), is(10));
    }

}