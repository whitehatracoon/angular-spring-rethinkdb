package de.whitehatracoons.todoList.util;

import de.whitehatracoons.todoList.Application;
import de.whitehatracoons.todoList.entity.user.UserEntity;
import de.whitehatracoons.todoList.model.user.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by venance on 01.06.17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@ActiveProfiles("test")
public class MapperTest {

    @Autowired
    private Mapper mapper;

    @Test
    public void mapUserToUserEntity() {
        User user = new User("Jaro", "jaro@whitehatracoons.de", "secret");
        user.setUserId("cikcmwkl-smcls-csmkc");

        UserEntity actual = mapper.map(user, UserEntity.class);

        assertThat(actual, notNullValue());
        assertThat(actual.getUserId(), is(user.getUserId()));
        assertThat(actual.getUserName(), is(user.getUserName()));
        assertThat(actual.getEmail(), is(user.getEmail()));
        assertThat(actual.getPassword(), is(user.getPassword()));
    }

    @Test
    public void mapUserEntityToUser() {
        UserEntity userEntity = new UserEntity();
        userEntity.setUserId("wncjns-sccdnsj-csdjnsk");
        userEntity.setUserName("Jaro");
        userEntity.setEmail("jaro@whitehatracoons.de");
        userEntity.setPassword("secret");

        User actual = mapper.map(userEntity, User.class);

        assertThat(actual, notNullValue());
        assertThat(actual.getUserId(), is(userEntity.getUserId()));
        assertThat(actual.getUserName(), is(userEntity.getUserName()));
        assertThat(actual.getEmail(), is(userEntity.getEmail()));
        assertThat(actual.getPassword(), is(userEntity.getPassword()));
    }

}