package Util;

import com.rethinkdb.net.Connection;
import de.whitehatracoons.todoList.database.RethinkDBConnectionFactory;
import de.whitehatracoons.todoList.model.task.Task;
import de.whitehatracoons.todoList.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.TestComponent;

import static com.rethinkdb.RethinkDB.r;

/**
 * Created by venance on 25.06.17.
 */
public class TestHelper {

    public static void cleanDatabaseTable(String database, String table, RethinkDBConnectionFactory rethinkDBConnectionFactory) {
        Connection connection = rethinkDBConnectionFactory.createConnection();
        r.db(database).table(table).delete().run(connection);
        connection.close();
    }

    public static User getTestUser() {
        Task task = new Task("test task", "this is a task for testing purposes");
        User user = new User("Jaro", "jaro@whitehatracoons.de", "secret");
        user.addTask(task);
        return user;
    }

    public static Task getTestTask() {
        return new Task("User Service Task", "This is a test task for the user service tests");
    }

}
