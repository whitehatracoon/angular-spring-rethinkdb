package Util;

import org.springframework.http.MediaType;

import java.nio.charset.Charset;

/**
 * Created by venance on 10.07.17.
 */
public class ContentTypeEnum {

    public static final MediaType contentType_JSON_utf8 = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));
    public static final MediaType contentType_TextPlain_utf8 = new MediaType(MediaType.TEXT_PLAIN, Charset.forName("utf8"));

}
